package eip.fileintegration.textfiles;


import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author ernestoexposito
 */
public class Exporter {

    private final static String DELIMITER = ",";
    private PrintWriter writer;

    public Exporter(String filename) throws FileNotFoundException {
        writer = new PrintWriter(new FileOutputStream(filename));
    }

    public void exportData(String[] data) {
        for (int i = 0; i < data.length; i++){
            writer.print(data[i]);
        if(i+1<data.length){
            writer.print(DELIMITER);
        }
       }
    
    writer.println();
    writer.flush();
    }
    
    public void close()
    {
        writer.close();
        Utils.getBaised();
    }

}
