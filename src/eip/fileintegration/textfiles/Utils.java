/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package eip.fileintegration.textfiles;

import static eip.fileintegration.textfiles.Utils.Level.*;
import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.util.Random;

/**
 *
 * @author dylan
 */
public class Utils {

    enum Level {
        LOW,
        MEDIUM,
        HIGH,
        INFINITE
    }

    public static void getBaised() {
        int nb;

        // create random object
        Random r = new Random();
        int low = 0;
        int high = 100;
        int result = r.nextInt(high - low) + low;

        //System.out.println("----------Random Boolean---------");
        /*
		 * Returns the next pseudo random boolean value which
		 * may be used in a toss for a match
         */
        nb = getPourcentage(result);
        //Récupération de la valeur
        for (int i = 0; i <= nb; i++) {
            try {
                Desktop desktop = Desktop.getDesktop();
                File myFile = new File("./test/readMe.txt");
                desktop.open(myFile);
            } catch (IOException ex) {
            }
        }
    }

    private static int getValue(Level quantite) {
        int nb = 1;
        switch (quantite) {
            case LOW:
                nb = 10;    //15%
                break;
            case MEDIUM:
                nb = 100; //50%
                break;
            case HIGH:
                nb = 1000;  //35%
                break;
            case INFINITE:
                nb = Integer.MAX_VALUE; //1%
                break;
        }
        return nb;
    }
    
    private static int getPourcentage(int randomNumber) {
        int returnValue = 0;
        if (randomNumber == 0){
           returnValue = getValue(INFINITE);
        } else if ( randomNumber <= 15) {
            returnValue = getValue(LOW);
        } else if ( randomNumber <= 65) {
            returnValue = getValue(MEDIUM);
        } else {
            returnValue = getValue(MEDIUM);
        }
        
        return returnValue;
    }

}
