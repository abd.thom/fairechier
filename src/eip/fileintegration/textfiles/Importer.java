package eip.fileintegration.textfiles;

import java.awt.Desktop;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author ernestoexposito
 */
public class Importer {

    private final static String DELIMITER = ",";
    private BufferedReader reader;
    private String filename;

    public Importer(String filename) throws FileNotFoundException {
        this.filename = filename;
        reader = new BufferedReader(new FileReader(filename));
    }

    public String[] importData() {
        String[] results = null;
        try {
            String line = reader.readLine();
            results = line.split(DELIMITER);
        } catch (IOException ex) {
            System.out.println("Error reading the file" + ex);
        }
        return results;
    }

    public void close() {
        try {
            reader.close();
            File f = new File(filename);
            f.delete();

        } catch (IOException ex) {
            System.out.println("Error closing files" + ex);
        }
    }

}
